# 1. Build the binary
FROM golang:1.12 AS BUILDER

WORKDIR /inventions-registry
COPY . .
RUN make build-unix

# 2. Package the binary in a container
FROM scratch
ARG INVENTIONS_FILE_PATH=./docs/inventions.json

COPY --from=BUILDER /inventions-registry/inventions_registry_unix.exe .
COPY ${INVENTIONS_FILE_PATH} ./inventions.json

ENV PORT 8080
ENV GIN_MODE release

EXPOSE 8080

ENTRYPOINT ["./inventions_registry_unix.exe"]
