package configuration

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func Load() {
	viper.SetDefault("INVENTIONS_FILE_PATH", "inventions.json")

	viper.AutomaticEnv()

	log.WithField("path of inventions file", GetInventionsFilePath()).Info("loaded configuration")
}

func GetInventionsFilePath() string {
	return viper.GetString("INVENTIONS_FILE_PATH")
}