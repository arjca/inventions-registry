# Inventions registry

*You may find the specifications of the service in the [docs](docs) directory.*

## Build & run

- If you are on a system with ```make```, you can find all available commands with ```make help```.
- To build the Unix specific binary runner, type ```make build-unix``` or ```CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o inventions_registry_unix.exe -v```.
- To package the whole in a Docker image, you do not need to build the binary first, just type ```docker build -t inventions-registry .```.

- Run the built image with ```docker run -p 8080:8080 inventions-registry```.
- The previous command bind the listened port to localhost:8080. If you use Postman, you can load both the [requests directory](postman_requests.json) and the [environment](postman_environment.json) which it uses.

