package inventions

type Dao interface {
	Create(Invention) (InventionID, error)

	ReadAll() ([]Invention, error)
	ReadAllWithTag(string) ([]Invention, error)
	ReadOne(InventionID) (Invention, error)
	ReadMostSimilar(InventionID) (Invention, error)

	DeleteOne(InventionID) error
	DeleteAll() error
}
