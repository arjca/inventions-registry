package inventions

type InventionID int

type Invention struct {
	ID       InventionID `json:"id,omitempty"`
	Year     int         `json:"date,omitempty"`
	Name     string      `json:"name,omitempty"`
	Inventor string      `json:"inventor,omitempty"`
	Origin   string      `json:"origin,omitempty"`
	Site     string      `json:"site,omitempty"`
	Tags     []string    `json:"tags,omitempty"`
}
