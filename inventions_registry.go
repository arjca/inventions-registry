package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/aubeantoine/inventions-registry/configuration"
	"gitlab.com/aubeantoine/inventions-registry/inventions"
	"gitlab.com/aubeantoine/inventions-registry/registry"
)

func main() {
	configuration.Load()

	dao := inventions.NewMapDao()
	base := registry.NewFileInventionsBase(configuration.GetInventionsFilePath())

	serviceController := registry.NewController(dao, base)

	router := registry.NewRouter(serviceController)

	if err := router.Run(); err != nil {
		log.WithError(err).
			Fatal("error while running the inventions registry")
	}
}
