module gitlab.com/aubeantoine/inventions-registry

go 1.12

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/gin-contrib/sse v0.0.0-20190301062529-5545eab6dad3 // indirect
	github.com/gin-gonic/gin v1.3.0
	github.com/golang/protobuf v1.3.0 // indirect
	github.com/json-iterator/go v1.1.6 // indirect
	github.com/mattn/go-isatty v0.0.7 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/sirupsen/logrus v1.4.0
	github.com/spf13/viper v1.3.2
	github.com/stretchr/testify v1.3.0
	github.com/ugorji/go/codec v0.0.0-20190309163734-c4a1c341dc93 // indirect
	golang.org/x/net v0.0.0-20190313082753-5c2c250b6a70 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v8 v8.18.2 // indirect
)
