package registry

import (
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"net/http"
)

func NewRouter(registryController Controller) *gin.Engine {
	router := gin.Default()

	if err := registryController.Initialize(); err != nil {
		log.WithError(err).Error("failed to initialize registry")
	}

	router.POST("/inventions", registryController.CreateOneInvention)
	router.PUT("/inventions/init", registryController.InitializeInventions)

	router.GET("/inventions", registryController.ReadAllInventions)
	router.GET("/inventions/:id", registryController.ReadOneInvention)
	// TODO Find another simpler way to implement routes like "/inventions/tag/:tag".
	// Created a less specific route because of a limitation of *httprouter*.
	router.GET("/inventions/:id/:parameter", func(ctx *gin.Context) {
		routeInventionWithTwoParameters(ctx, registryController)
	})

	router.DELETE("/inventions/:id", registryController.DeleteOneInvention)

	return router
}

func routeInventionWithTwoParameters(ctx *gin.Context, controller Controller) {
	firstParameter := ctx.Param("id")
	secondParameter := ctx.Param("parameter")

	switch firstParameter {
	case "tag":
		controller.ReadAllInventionsWithTag(ctx, secondParameter)
	default:
		switch secondParameter {
		case "discovery":
			controller.FindMostSimilarInvention(ctx, firstParameter)
		}
		ctx.Status(http.StatusNotFound)
	}
}
