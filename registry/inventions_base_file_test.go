package registry

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/aubeantoine/inventions-registry/inventions"
	"testing"
)

func TestFileInventionsBase_List(t *testing.T) {
	base := NewFileInventionsBase("test_inventions.json")

	found, err := base.List()

	assert.Nil(t, err)
	assert.Len(t, found, 2)

	assert.Equal(t, inventions.Invention{
		Year:     1862,
		Name:     "Les Misérables",
		Inventor: "Victor Hugo",
		Origin:   "France",
		Tags:     []string{"Papier", "Livre", "Ecriture", "Histoire", "Auteur"},
	}, found[0])
	assert.Equal(t, inventions.Invention{
		Year:     1896,
		Name:     "L'invention du tube de dentifrice",
		Inventor: "Colgate & Company",
		Origin:   "Royaume-Uni",
	}, found[1])
}

func TestFileInventionsBase_ListNotExistingFile(t *testing.T) {
	base := NewFileInventionsBase("test_inventions_not_existing.json")

	_, err := base.List()

	assert.NotNil(t, err)
}

func TestFileInventionsBase_ListMalformedJsonInFile(t *testing.T) {
	base := NewFileInventionsBase("test_malformed_inventions.txt")

	_, err := base.List()

	assert.NotNil(t, err)
}
