package registry

import (
	"bytes"
	"encoding/json"
	"errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"gitlab.com/aubeantoine/inventions-registry/inventions"
	"net/http"
	"net/http/httptest"
	"testing"
)

type mockDao struct {
	mock.Mock
}

func (dao *mockDao) ReadMostSimilar(ID inventions.InventionID) (inventions.Invention, error) {
	args := dao.Called(ID)

	return args.Get(0).(inventions.Invention), args.Error(1)
}

func (dao *mockDao) ReadAllWithTag(tag string) ([]inventions.Invention, error) {
	args := dao.Called(tag)

	return args.Get(0).([]inventions.Invention), args.Error(1)
}

func (dao *mockDao) DeleteAll() error {
	args := dao.Called()

	return args.Error(0)
}

func (dao *mockDao) DeleteOne(ID inventions.InventionID) error {
	args := dao.Called(ID)

	return args.Error(0)
}

func (dao *mockDao) ReadAll() ([]inventions.Invention, error) {
	args := dao.Called()

	return args.Get(0).([]inventions.Invention), args.Error(1)
}

func (dao *mockDao) ReadOne(ID inventions.InventionID) (inventions.Invention, error) {
	args := dao.Called(ID)

	return args.Get(0).(inventions.Invention), args.Error(1)
}

func (dao *mockDao) Create(invention inventions.Invention) (inventions.InventionID, error) {
	args := dao.Called(invention)

	return args.Get(0).(inventions.InventionID), args.Error(1)
}

type mockInventionsBase struct {
	mock.Mock
}

func (base *mockInventionsBase) List() ([]inventions.Invention, error) {
	args := base.Called()

	return args.Get(0).([]inventions.Invention), args.Error(1)
}

func TestRouter_CreateOneInvention(t *testing.T) {
	dao := new(mockDao)
	dao.On("Create", mock.Anything).Return(inventions.InventionID(1), nil)
	dao.On("ReadOne", inventions.InventionID(1)).Return(inventions.Invention{
		ID:       inventions.InventionID(1),
		Year:     1879,
		Name:     "L'invention de l'ampoule électrique",
		Inventor: "Thomas Edison",
	}, nil)

	router := NewRouter(NewController(dao, nil))

	inventionToCreate := inventions.Invention{
		Year:     1879,
		Name:     "L'invention de l'ampoule électrique",
		Inventor: "Thomas Edison",
	}

	jsonInvention, _ := json.Marshal(inventionToCreate)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/inventions", bytes.NewBuffer(jsonInvention))
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusCreated, w.Code)

	createdInvention := inventions.Invention{}
	assert.Nil(t, json.Unmarshal(w.Body.Bytes(), &createdInvention))

	assert.Equal(t, inventions.Invention{
		ID:       inventions.InventionID(1),
		Year:     1879,
		Name:     "L'invention de l'ampoule électrique",
		Inventor: "Thomas Edison",
	}, createdInvention)
}

func TestRouter_CreateOneInventionWhenDaoFailing(t *testing.T) {
	dao := new(mockDao)
	dao.On("Create", mock.Anything).Return(inventions.InventionID(0), errors.New("fake error"))

	router := NewRouter(NewController(dao, nil))

	inventionToCreate := inventions.Invention{
		Year:     1879,
		Name:     "L'invention de l'ampoule électrique",
		Inventor: "Thomas Edison",
	}

	jsonInvention, _ := json.Marshal(inventionToCreate)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/inventions", bytes.NewBuffer(jsonInvention))
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusInternalServerError, w.Code)
}

func TestRouter_CreateOneInventionFailingWhenSendAnotherObject(t *testing.T) {
	dao := new(mockDao)

	router := NewRouter(NewController(dao, nil))

	sentBody := 2

	jsonInvention, _ := json.Marshal(sentBody)

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("POST", "/inventions", bytes.NewBuffer(jsonInvention))
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestRouter_ReadAllInventions(t *testing.T) {
	dao := new(mockDao)
	dao.On("ReadAll").Return([]inventions.Invention{
		{
			ID:       inventions.InventionID(1),
			Year:     1879,
			Name:     "L'invention de l'ampoule électrique",
			Inventor: "Thomas Edison",
		},
		{
			ID:       inventions.InventionID(2),
			Year:     1938,
			Name:     "L'invention du stylo à bille",
			Inventor: "László Biró",
			Origin:   "Hongrie",
			Tags:     []string{"Papier", "Dessin", "Ecriture", "Plastique", "Histoire"},
		},
	}, nil)

	router := NewRouter(NewController(dao, nil))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/inventions", nil)

	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	var found []inventions.Invention
	assert.Nil(t, json.Unmarshal(w.Body.Bytes(), &found))

	assert.Len(t, found, 2)

	assert.Equal(t, inventions.Invention{
		ID:       inventions.InventionID(1),
		Year:     1879,
		Name:     "L'invention de l'ampoule électrique",
		Inventor: "Thomas Edison",
	}, found[0])
	assert.Equal(t, inventions.Invention{
		ID:       inventions.InventionID(2),
		Year:     1938,
		Name:     "L'invention du stylo à bille",
		Inventor: "László Biró",
		Origin:   "Hongrie",
		Tags:     []string{"Papier", "Dessin", "Ecriture", "Plastique", "Histoire"},
	}, found[1])
}

func TestRouter_ReadAllInventionsFailingBecauseOfTheDao(t *testing.T) {
	dao := new(mockDao)
	dao.On("ReadAll").Return([]inventions.Invention{}, errors.New("fake error"))

	router := NewRouter(NewController(dao, nil))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/inventions", nil)

	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusInternalServerError, w.Code)
}

func TestRouter_ReadOneInvention(t *testing.T) {
	dao := new(mockDao)
	dao.On("ReadOne", inventions.InventionID(1)).Return(inventions.Invention{
		ID:       inventions.InventionID(1),
		Year:     1879,
		Name:     "L'invention de l'ampoule électrique",
		Inventor: "Thomas Edison",
	}, nil)

	router := NewRouter(NewController(dao, nil))

	w := httptest.NewRecorder()

	req, _ := http.NewRequest("GET", "/inventions/1", nil)

	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	found := inventions.Invention{}
	assert.Nil(t, json.Unmarshal(w.Body.Bytes(), &found))
	assert.Equal(t, inventions.Invention{
		ID:       inventions.InventionID(1),
		Year:     1879,
		Name:     "L'invention de l'ampoule électrique",
		Inventor: "Thomas Edison",
	}, found)
}

func TestRouter_ReadOneInventionFailingWhenNotExists(t *testing.T) {
	dao := new(mockDao)
	dao.On("ReadOne", inventions.InventionID(1)).Return(inventions.Invention{}, errors.New("invention not found"))

	router := NewRouter(NewController(dao, nil))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/inventions/1", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusNotFound, w.Code)
	assert.Equal(t, "\"invention not found\"", w.Body.String())
}

func TestRouter_ReadOneInventionFailingToResolveId(t *testing.T) {
	dao := new(mockDao)

	router := NewRouter(NewController(dao, nil))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/inventions/a", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestRouter_DeleteOneInvention(t *testing.T) {
	dao := new(mockDao)
	dao.On("DeleteOne", inventions.InventionID(1)).Return(nil)

	router := NewRouter(NewController(dao, nil))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "/inventions/1", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)
}

func TestRouter_DeleteOneInventionWhenNotExisting(t *testing.T) {
	dao := new(mockDao)
	dao.On("DeleteOne", inventions.InventionID(1)).Return(errors.New("invention not found"))

	router := NewRouter(NewController(dao, nil))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "/inventions/1", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusNotFound, w.Code)
}

func TestRouter_DeleteOneInventionWithDaoFailure(t *testing.T) {
	dao := new(mockDao)
	dao.On("DeleteOne", inventions.InventionID(1)).Return(errors.New("fake error"))

	router := NewRouter(NewController(dao, nil))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "/inventions/1", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusInternalServerError, w.Code)
}

func TestRouter_DeleteOneInventionFailingToResolveId(t *testing.T) {
	dao := new(mockDao)

	router := NewRouter(NewController(dao, nil))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("DELETE", "/inventions/a", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusBadRequest, w.Code)
}

func TestRouter_InitializationThenReadAll(t *testing.T) {
	dao := inventions.NewMapDao()

	base := new(mockInventionsBase)
	base.On("List").Return([]inventions.Invention{
		{
			Year:     1879,
			Name:     "L'invention de l'ampoule électrique",
			Inventor: "Thomas Edison",
		},
		{
			Year:     1938,
			Name:     "L'invention du stylo à bille",
			Inventor: "László Biró",
			Origin:   "Hongrie",
			Tags:     []string{"Papier", "Dessin", "Ecriture", "Plastique", "Histoire"},
		},
	}, nil)

	router := NewRouter(NewController(dao, base))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/inventions", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	var found []inventions.Invention
	assert.Nil(t, json.Unmarshal(w.Body.Bytes(), &found))

	assert.Len(t, found, 2)

	assert.Equal(t, inventions.Invention{
		ID:       1,
		Year:     1879,
		Name:     "L'invention de l'ampoule électrique",
		Inventor: "Thomas Edison",
	}, found[0])
	assert.Equal(t, inventions.Invention{
		ID:       2,
		Year:     1938,
		Name:     "L'invention du stylo à bille",
		Inventor: "László Biró",
		Origin:   "Hongrie",
		Tags:     []string{"Papier", "Dessin", "Ecriture", "Plastique", "Histoire"},
	}, found[1])
}

func TestRouter_ReinitializationErasesPrevious(t *testing.T) {
	dao := inventions.NewMapDao()

	base := new(mockInventionsBase)
	base.On("List").Return([]inventions.Invention{
		{
			Year:     1879,
			Name:     "L'invention de l'ampoule électrique",
			Inventor: "Thomas Edison",
		},
		{
			Year:     1938,
			Name:     "L'invention du stylo à bille",
			Inventor: "László Biró",
			Origin:   "Hongrie",
			Tags:     []string{"Papier", "Dessin", "Ecriture", "Plastique", "Histoire"},
		},
	}, nil)

	_, _ = dao.Create(inventions.Invention{
		Year:     1896,
		Name:     "L'invention du tube de dentifrice",
		Inventor: "Colgate & Company",
		Origin:   "Royaume-Uni",
	})

	router := NewRouter(NewController(dao, base))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("PUT", "/inventions/init", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusCreated, w.Code)

	w = httptest.NewRecorder()
	req, _ = http.NewRequest("GET", "/inventions", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	var found []inventions.Invention
	assert.Nil(t, json.Unmarshal(w.Body.Bytes(), &found))

	assert.Len(t, found, 2)

	assert.Equal(t, inventions.Invention{
		ID:       1,
		Year:     1879,
		Name:     "L'invention de l'ampoule électrique",
		Inventor: "Thomas Edison",
	}, found[0])
	assert.Equal(t, inventions.Invention{
		ID:       2,
		Year:     1938,
		Name:     "L'invention du stylo à bille",
		Inventor: "László Biró",
		Origin:   "Hongrie",
		Tags:     []string{"Papier", "Dessin", "Ecriture", "Plastique", "Histoire"},
	}, found[1])
}

func TestRouter_ReadAllWithTag(t *testing.T) {
	dao := new(mockDao)
	dao.On("ReadAllWithTag", "Papier").Return([]inventions.Invention{
		{
			ID:       inventions.InventionID(5),
			Year:     1795,
			Name:     "L'invention du crayon graphite",
			Inventor: "Nicolas-Jacques Conté",
			Origin:   "France",
			Tags:     []string{"Papier", "Dessin", "Ecriture", "Illustration"},
		},
		{
			ID:       inventions.InventionID(18),
			Year:     1938,
			Name:     "L'invention du stylo à bille",
			Inventor: "László Biró",
			Origin:   "Hongrie",
			Tags:     []string{"Papier", "Dessin", "Ecriture", "Plastique", "Histoire"},
		},
	}, nil)

	router := NewRouter(NewController(dao, nil))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/inventions/tag/Papier", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	var found []inventions.Invention
	assert.Nil(t, json.Unmarshal(w.Body.Bytes(), &found))

	assert.Len(t, found, 2)

	assert.Equal(t, inventions.Invention{
		ID:       inventions.InventionID(5),
		Year:     1795,
		Name:     "L'invention du crayon graphite",
		Inventor: "Nicolas-Jacques Conté",
		Origin:   "France",
		Tags:     []string{"Papier", "Dessin", "Ecriture", "Illustration"},
	}, found[0])
	assert.Equal(t, inventions.Invention{
		ID:       inventions.InventionID(18),
		Year:     1938,
		Name:     "L'invention du stylo à bille",
		Inventor: "László Biró",
		Origin:   "Hongrie",
		Tags:     []string{"Papier", "Dessin", "Ecriture", "Plastique", "Histoire"},
	}, found[1])
}

func TestRouter_ReadAllWithTagEmptyOutput(t *testing.T) {
	dao := new(mockDao)
	dao.On("ReadAllWithTag", "Papier").Return([]inventions.Invention{}, nil)

	router := NewRouter(NewController(dao, nil))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/inventions/tag/Papier", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	var found []inventions.Invention
	assert.Nil(t, json.Unmarshal(w.Body.Bytes(), &found))

	assert.Equal(t, "[]", w.Body.String())
	assert.Len(t, found, 0)
}

func TestRouter_ReadAllWithTagWhenDaoFailing(t *testing.T) {
	dao := new(mockDao)
	dao.On("ReadAllWithTag", "abc").Return([]inventions.Invention{}, errors.New("fake error"))

	router := NewRouter(NewController(dao, nil))

	w := httptest.NewRecorder()
	req, _ := http.NewRequest("GET", "/inventions/tag/abc", nil)
	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusInternalServerError, w.Code)
}

func TestRouter_FindMostSimilar(t *testing.T) {
	dao := new(mockDao)
	dao.On("ReadMostSimilar", inventions.InventionID(1)).Return(inventions.Invention{
		ID:       inventions.InventionID(5),
		Year:     1795,
		Name:     "L'invention du crayon graphite",
		Inventor: "Nicolas-Jacques Conté",
		Origin:   "France",
		Tags:     []string{"Papier", "Dessin", "Ecriture", "Illustration"},
	}, nil)

	router := NewRouter(NewController(dao, nil))

	w := httptest.NewRecorder()

	req, _ := http.NewRequest("GET", "/inventions/1/discovery", nil)

	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusOK, w.Code)

	found := inventions.Invention{}
	assert.Nil(t, json.Unmarshal(w.Body.Bytes(), &found))
	assert.Equal(t, inventions.Invention{
		ID:       inventions.InventionID(5),
		Year:     1795,
		Name:     "L'invention du crayon graphite",
		Inventor: "Nicolas-Jacques Conté",
		Origin:   "France",
		Tags:     []string{"Papier", "Dessin", "Ecriture", "Illustration"},
	}, found)

}

func TestRouter_FindMostSimilarOfUnknown(t *testing.T) {
	dao := new(mockDao)
	dao.On("ReadMostSimilar", inventions.InventionID(1)).Return(inventions.Invention{}, errors.New("invention not found"))

	router := NewRouter(NewController(dao, nil))

	w := httptest.NewRecorder()

	req, _ := http.NewRequest("GET", "/inventions/1/discovery", nil)

	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusNotFound, w.Code)
	assert.Equal(t, "\"invention not found\"", w.Body.String())
}

func TestRouter_FindMostSimilarWithNoSimilar(t *testing.T) {
	dao := new(mockDao)
	dao.On("ReadMostSimilar", inventions.InventionID(1)).Return(inventions.Invention{}, errors.New("no similar invention found"))

	router := NewRouter(NewController(dao, nil))

	w := httptest.NewRecorder()

	req, _ := http.NewRequest("GET", "/inventions/1/discovery", nil)

	router.ServeHTTP(w, req)

	assert.Equal(t, http.StatusNoContent, w.Code)
}
