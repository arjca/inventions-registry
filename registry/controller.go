package registry

import (
	"fmt"
	"github.com/gin-gonic/gin"
	log "github.com/sirupsen/logrus"
	"gitlab.com/aubeantoine/inventions-registry/inventions"
	"net/http"
	"strconv"
)

const inventionNotFoundMessage = "invention not found"

type Controller interface {
	Initialize() error
	InitializeInventions(*gin.Context)
	CreateOneInvention(*gin.Context)
	ReadAllInventions(*gin.Context)
	ReadOneInvention(*gin.Context)
	DeleteOneInvention(*gin.Context)

	ReadAllInventionsWithTag(*gin.Context, string)
	FindMostSimilarInvention(*gin.Context, string)
}

type controllerImplementation struct {
	dao            inventions.Dao
	inventionsBase InventionsBase
}

func (controller *controllerImplementation) FindMostSimilarInvention(ctx *gin.Context, uncheckedID string) {
	idToRetrieve, err := strconv.Atoi(uncheckedID)
	if err != nil {
		log.WithError(err).
			Error("failed to parse the researched id")

		ctx.JSON(http.StatusBadRequest, "id should be an integer")

		return
	}

	found, err := controller.dao.ReadMostSimilar(inventions.InventionID(idToRetrieve))
	if err != nil {
		log.WithError(err).
			Error("failed to retrieve similar invention")

		fmt.Println(err.Error())

		if err.Error() == inventionNotFoundMessage {
			ctx.JSON(http.StatusNotFound, err.Error())
		} else if err.Error() == "no similar invention found" {
			ctx.JSON(http.StatusNoContent, err.Error())
		} else {
			ctx.JSON(http.StatusInternalServerError, err)
		}

		return
	}

	ctx.JSON(http.StatusOK, found)
}

func (controller *controllerImplementation) ReadAllInventionsWithTag(ctx *gin.Context, tag string) {
	if tag == "" {
		log.Error("no tag specified")

		ctx.JSON(http.StatusBadRequest, "no tag specified")

		return
	}

	found, err := controller.dao.ReadAllWithTag(tag)
	if err != nil {
		log.WithError(err).
			Error("failed to read the inventions through the DAO")

		ctx.JSON(http.StatusInternalServerError, "could not read the inventions")

		return
	}

	ctx.JSON(http.StatusOK, found)
}

func (controller *controllerImplementation) Initialize() error {
	if controller.inventionsBase == nil {
		// Nothing to initialize.
		return nil
	}

	if err := controller.dao.DeleteAll(); err != nil {
		return err
	}

	foundInventions, err := controller.inventionsBase.List()
	if err != nil {
		return err
	}

	for _, invention := range foundInventions {
		if _, err := controller.dao.Create(invention); err != nil {
			return err
		}
	}

	return nil
}

func (controller *controllerImplementation) InitializeInventions(ctx *gin.Context) {
	if err := controller.Initialize(); err != nil {
		log.WithError(err).
			Error("failed to initialize inventions")

		ctx.JSON(http.StatusInternalServerError, "failed to completely initialize inventions")

		return
	}

	ctx.JSON(http.StatusCreated, "registry has been initialized!")
}

func (controller *controllerImplementation) DeleteOneInvention(ctx *gin.Context) {
	idParameter := ctx.Param("id")

	idToRetrieve, err := strconv.Atoi(idParameter)
	if err != nil {
		log.WithError(err).
			Error("failed to parse the researched id")

		ctx.JSON(http.StatusBadRequest, "id should be an integer")

		return
	}

	if err = controller.dao.DeleteOne(inventions.InventionID(idToRetrieve)); err != nil {
		switch err.Error() {
		case inventionNotFoundMessage:
			log.WithError(err).
				Error("failed to delete the invention through the DAO because the invention was not found")

			ctx.JSON(http.StatusNotFound, inventionNotFoundMessage)

		default:
			log.WithError(err).
				Error("failed to delete the invention through the DAO")

			ctx.JSON(http.StatusInternalServerError, "could not delete the invention")
		}

		return
	}

	ctx.JSON(http.StatusOK, "invention deleted")
}

func (controller *controllerImplementation) ReadAllInventions(ctx *gin.Context) {
	storedInventions, err := controller.dao.ReadAll()
	if err != nil {
		log.WithError(err).
			Error("failed to read the inventions through the DAO")

		ctx.JSON(http.StatusInternalServerError, "could not read the inventions")

		return
	}

	ctx.JSON(http.StatusOK, storedInventions)
}

func (controller *controllerImplementation) ReadOneInvention(ctx *gin.Context) {
	idParameter := ctx.Param("id")

	idToRetrieve, err := strconv.Atoi(idParameter)
	if err != nil {
		log.WithError(err).
			Error("failed to parse the researched id")

		ctx.JSON(http.StatusBadRequest, "id should be an integer")

		return
	}

	found, err := controller.dao.ReadOne(inventions.InventionID(idToRetrieve))
	if err != nil {
		log.WithError(err).
			Error("failed to retrieve invention")

		if err.Error() == inventionNotFoundMessage {
			ctx.JSON(http.StatusNotFound, err.Error())
		} else {
			ctx.JSON(http.StatusInternalServerError, err)
		}

		return
	}

	ctx.JSON(http.StatusOK, found)
}

func (controller *controllerImplementation) CreateOneInvention(ctx *gin.Context) {
	var inventionToCreate *inventions.Invention
	if err := ctx.BindJSON(&inventionToCreate); err != nil {
		log.WithError(err).
			Error("failed to parse the body into an invention")

		ctx.JSON(http.StatusBadRequest, "the body does not contain an invention")

		return
	}

	createdID, err := controller.dao.Create(*inventionToCreate)
	if err != nil {
		log.WithError(err).
			WithField("invention", inventionToCreate).
			Error("failed to create the invention through the DAO")

		ctx.JSON(http.StatusInternalServerError, "the body does not contain an invention")

		return
	}

	found, err := controller.dao.ReadOne(createdID)
	if err != nil {
		log.WithError(err).
			Error("failed to retrieve invention")

		ctx.JSON(http.StatusInternalServerError, err)

		return
	}

	ctx.JSON(http.StatusCreated, found)
}

func NewController(dao inventions.Dao, initializer InventionsBase) Controller {
	return &controllerImplementation{
		dao:            dao,
		inventionsBase: initializer,
	}
}
