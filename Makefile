PROJECT_NAME := "inventions-registry"
PKG_LIST := $(shell go list ./...)
GO_FILES := $(shell find . -name '*.go' | grep -v _test.go)
COVERAGE_FILE := "coverage.out"
COVERAGE_HTML_FILE := "coverage.html"
UNIX_EXECUTABLE_NAME := "inventions_registry_unix.exe"
GOLANGCI_LINT_VERSION := "1.15.0"

.PHONY: build clean test coverhtml lint

all: build-unix

dep:
	@which golangci-lint || curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b $(shell go env GOPATH)/bin v${GOLANGCI_LINT_VERSION}

lint: ## Lint the files (needs local installation of golangci-lint)
	@golangci-lint run --enable=deadcode  --enable=gocyclo --enable=golint --enable=varcheck --enable=structcheck --enable=maligned --enable=errcheck --enable=dupl --enable=ineffassign --enable=interfacer --enable=unconvert --enable=goconst --enable=gosec --enable=megacheck

test: ## Run unittests
	@go test -short -covermode=count -coverprofile=${COVERAGE_FILE} ${PKG_LIST}

coverage: $(COVERAGE_FILE) ## Generate a raw file for code coverage
$(COVERAGE_FILE):
	@go test -covermode=count -coverprofile=${COVERAGE_FILE} ${PKG_LIST}

coverhtml: $(COVERAGE_FILE) ## Generate an HTML file for code coverage
	@go tool cover -html=${COVERAGE_FILE} -o ${COVERAGE_HTML_FILE}

build-unix: ## Build the standalone binary file which can be run on a unix system
	@CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o ${UNIX_EXECUTABLE_NAME} -v

clean: ## Remove previous build
	@rm -f $(PROJECT_NAME) $(COVERAGE_FILE) $(COVERAGE_HTML_FILE) $(UNIX_EXECUTABLE_NAME)

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'